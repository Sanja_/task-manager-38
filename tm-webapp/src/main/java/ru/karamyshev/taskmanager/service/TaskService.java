package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.TaskDTO;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.empty.IdEmptyException;
import ru.karamyshev.taskmanager.exception.empty.NameEmptyException;
import ru.karamyshev.taskmanager.repository.ITaskRepository;

import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Override
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String taskName,
            @Nullable final String projectName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskName == null || taskName.isEmpty()) throw new NameEmptyException();
        if (projectName == null || projectName.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = new Task();
        task.setName(taskName);
        task.setUser(userService.findById(userId));
        task.setProject(projectService.findOneByName(userId, projectName));
        taskRepository.save(task);

    }

    @Override
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String taskName,
            @Nullable final String projectName,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskName == null || taskName.isEmpty()) throw new NameEmptyException();
        if (projectName == null || projectName.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new Exception();

        @Nullable final Task task = new Task();
        task.setName(taskName);
        task.setUser(userService.findById(userId));
        task.setProject(projectService.findOneByName(userId, projectName));
        task.setDescription(description);
        taskRepository.save(task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final List<Task> taskDTOList = taskRepository.findAllByUserId(userId);
        return TaskDTO.toDTO(taskDTOList);
    }

    @Transactional
    public void clearTaskByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteById(userId);
    }

    @NotNull
    @Override
    public Task findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final Task task = taskRepository.findByUserIdAndName(userId, name);
        return task;
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @Nullable List<Task> taskList = taskRepository.findAllByProjectId(projectId);
        return taskList;
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Task task = taskRepository.findByUserIdAndId(userId, id);
        return task;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final List<Task> taskList = taskRepository.findAll();
        return TaskDTO.toDTO(taskList);
    }

    @Nullable
    @Override
    @Transactional
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new Exception();
        @Nullable final Task task = taskRepository.findByUserIdAndId(userId, id);
        if (task == null) return;
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
    }

    @Nullable
    @Override
    @Transactional
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        taskRepository.deleteByUserIdAndName(userId, name);
    }

    @Nullable
    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void create(@Nullable Task task) throws Exception {
        if (task == null) throw new Exception();
        taskRepository.save(task);
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Task task = taskRepository.findById(id).orElse(null);
        return task;
    }

    @Override
    @Transactional
    public void updateTaskById(
            @Nullable final String id,
            @Nullable final Task task
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (task == null) throw new NameEmptyException();
        @Nullable final Task upTask = taskRepository.findById(id).orElse(null);
        if (upTask == null) return;
        upTask.setName(task.getName());
        upTask.setDescription(task.getDescription());
        taskRepository.save(upTask);
    }

}
