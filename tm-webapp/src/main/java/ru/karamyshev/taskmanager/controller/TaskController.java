package ru.karamyshev.taskmanager.controller;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.dto.ProjectDTO;
import ru.karamyshev.taskmanager.dto.TaskDTO;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.repository.ITaskRepository;

import java.util.List;

@RestController
@RequestMapping(value = "/task")
public class TaskController {

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired

    private ITaskService taskService;

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> findAll() {
        return taskService.findAll();
    }

    @DeleteMapping(value = "/delete/{id}")
    public void delete(@PathVariable("id") @Nullable String id) throws Exception {
        taskService.removeOneById(id);
    }

    @PostMapping(value = "/create")
    public void create(
            @RequestBody @Nullable final Task task
    ) throws Exception {
        taskService.create(task);
    }

    @GetMapping(value = "/view/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    public TaskDTO view(@PathVariable("id") String id) throws Exception {
        @Nullable final Task task = taskService.findById(id);
        return TaskDTO.toDTO(task);
    }

    @PostMapping("/edit/{id}")
    public void edit(
            @RequestParam("id") @Nullable final String projectId,
            @RequestBody @Nullable Task task
    ) throws Exception {
        taskService.updateTaskById(projectId, task);
    }

}
