package ru.karamyshev.taskmanager.constant;

public interface TimeConstant {
    String DATA_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";
    String TIME_ZONE = "Europe/Moscow";
}
