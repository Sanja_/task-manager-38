package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Task;

import java.util.List;


public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    Task findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable
    List<Task> findAllByProjectId(@NotNull String projectId);

    @NotNull
    Task deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    Task deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

}
