package ru.karamyshev.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
public class Task extends AbstractEntity {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private Date startData;

    @Nullable
    private Date finishData;

    @Nullable
    @Column(updatable = false)
    private Date creationData = new Date(System.currentTimeMillis());

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @ManyToOne
    private Project project;

}
