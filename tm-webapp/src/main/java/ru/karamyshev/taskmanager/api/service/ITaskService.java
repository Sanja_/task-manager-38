package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.karamyshev.taskmanager.dto.TaskDTO;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Task;

import java.util.List;

public interface ITaskService {

    void create(
            @Nullable String userId,
            @Nullable String taskName,
            @Nullable String projectName
    ) throws Exception;

    void create(
            @Nullable String userId,
            @Nullable String taskName,
            @Nullable String projectName,
            @Nullable String description
    ) throws Exception;

    @Nullable List<TaskDTO> findAllByUserId(@Nullable String userId);

    void clearTaskByUserId(@Nullable String userId);

    @NotNull Task findOneByName(
            @Nullable String userId,
            @Nullable String name
    );

    @Nullable List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable List<TaskDTO> findAll();

    void updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void removeOneByName(@Nullable String userId, @Nullable String name);

    void removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    void removeAll();

    void removeOneById(@Nullable String id) throws Exception;

    @Transactional
    void create(@Nullable Task task) throws Exception;

    @Nullable Task findById(@Nullable String id) throws Exception;

    void updateTaskById(
            @Nullable String taskId,
            @Nullable Task task
    ) throws Exception;

}
