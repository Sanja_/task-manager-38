package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.karamyshev.taskmanager.dto.ProjectDTO;
import ru.karamyshev.taskmanager.entity.Project;

import java.util.List;

public interface IProjectService {

    void create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable List<ProjectDTO> findAllByUserId(@Nullable String userId);

    @Nullable List<ProjectDTO> findAll();

    void clearAllByUserId(@Nullable String userId);

    @Nullable Project findOneByName(
            @Nullable String userId,
            @Nullable String name
    );

    void updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void removeOneByName(
            @Nullable String userId,
            @Nullable String name
    );

    @Nullable Project findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void removeOneById(@Nullable String id) throws Exception;

    void removeAll();

    @Nullable Project findById(@Nullable String id) throws Exception;

    void updateProjectById(
            @Nullable String id,
            @Nullable Project project
    );

    void create(@Nullable Project project) throws Exception;

}
