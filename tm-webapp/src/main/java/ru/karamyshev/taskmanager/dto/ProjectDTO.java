package ru.karamyshev.taskmanager.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.constant.TimeConstant;
import ru.karamyshev.taskmanager.entity.Project;

import java.io.Serializable;
import java.util.*;


@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ProjectDTO extends AbstractEntityDTO implements Serializable, TimeConstant {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATA_PATTERN, timezone = TIME_ZONE)
    private Date startDate;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATA_PATTERN, timezone = TIME_ZONE)
    private Date finishDate;

    @Override
    public String toString() {
        return "Project: " + "Name = " + name + '\''
                + "Description = " + description;
    }

    public ProjectDTO(@Nullable final Project project) {
        if (project == null) return;
        id = project.getId();
        name = project.getName();
        if (project.getDescription() != null) description = project.getDescription();
        if (project.getStartData() != null) startDate = project.getStartData();
        if (project.getFinishData() != null) finishDate = project.getFinishData();
        if (project.getUser().getId() != null) userId = project.getUser().getId();
    }

    public static ProjectDTO toDTO(@Nullable final Project project) {
        if (project == null) return null;
        return new ProjectDTO(project);
    }

    @NotNull
    public static List<ProjectDTO> toDTO(@Nullable final Collection<Project> projects) {
        if (projects == null || projects.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            result.add(new ProjectDTO(project));
        }
        return result;
    }

}
