package ru.karamyshev.taskmanager.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Task;

import java.io.Serializable;
import java.util.*;

import static ru.karamyshev.taskmanager.constant.TimeConstant.DATA_PATTERN;
import static ru.karamyshev.taskmanager.constant.TimeConstant.TIME_ZONE;

@Getter
@Setter
@NoArgsConstructor
public class TaskDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    private String userId;

    @NotNull
    private String projectId;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATA_PATTERN, timezone = TIME_ZONE)
    private Date startData;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATA_PATTERN, timezone = TIME_ZONE)
    private Date finishData;

    @Override
    public String toString() {
        return "Task: " + "Name = " + name + '\''
                + "Description = " + description;
    }

    @Nullable
    public static TaskDTO toDTO(@Nullable final Task task) {
        if (task == null) return null;
        return new TaskDTO(task);
    }

    @NotNull
    public static List<TaskDTO> toDTO(@Nullable final Collection<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            result.add(new TaskDTO(task));
        }
        return result;
    }

    public TaskDTO(@Nullable final Task task) {
        if (task == null) return;
        id = task.getId();
        name = task.getName();
        userId = task.getUser().getId();
        if (task.getProject() != null) projectId = task.getProject().getId();
        if (task.getDescription() != null) description = task.getDescription();
        if (task.getStartData() != null) startData = task.getStartData();
        if (task.getFinishData() != null) finishData = task.getFinishData();
    }

}
