package ru.karamyshev.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.lang.Exception;

@Component
public class RenameUserPasswordListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-rnm-psswrd";
    }

    @NotNull
    @Override
    public String command() {
        return "rename-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Rename password account.";
    }

    @Override
    @EventListener(condition = "@renameUserPasswordListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = sessionService.getSession();
        System.out.println("CHANGE ACCOUNT PASSWORD");
        System.out.println("[ENTER NEW PASSWORD]");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        userEndpoint.renamePassword(session, newPassword);
        System.out.println("[OK]");
    }

}
