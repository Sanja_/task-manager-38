package ru.karamyshev.taskmanager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.lang.Exception;
import java.util.List;

@Component
public class ProjectShowByNameListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-prtvwnm";
    }

    @NotNull
    @Override
    public String command() {
        return "project-view-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    @EventListener(condition = "@projectShowByNameListener.command() == #event.name")
    public void handler(ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = sessionService.getSession();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Project project = projectEndpoint.findOneProjectByName(session, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }

        showProjects(project);
        System.out.println("[OK]");
    }

    private void showProjects(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
    }

}
