package ru.karamyshev.taskmanager.listener.task;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;

import java.lang.Exception;
import java.util.List;

@Component
public class TasksShowListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-tsklst";
    }

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    @EventListener(condition = "@tasksShowListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = sessionService.getSession();
        System.out.println("[LIST TASKS]");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTaskByUserId(session);
        int index = 1;
        for (TaskDTO task : tasks) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}
