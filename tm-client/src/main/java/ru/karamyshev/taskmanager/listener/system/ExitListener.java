package ru.karamyshev.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.endpoint.SessionEndpoint;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class ExitListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    @EventListener(condition = "@exitListener.command() == #event.name")
    public void handler(ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = sessionService.getSession();
        sessionEndpoint.closeSession(session);
        sessionService.clearSession();
        System.exit(0);
    }

}
