package ru.karamyshev.taskmanager.listener.data.xml.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class DataJaxXmlSaveListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "data-jax-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data from xml file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJaxXmlSaveListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA XML(JAX-B) SAVE]");
        @Nullable final SessionDTO session = sessionService.getSession();
        adminEndpoint.saveDataJaxBXml(session);
        System.out.println("[OK]");
    }

}
