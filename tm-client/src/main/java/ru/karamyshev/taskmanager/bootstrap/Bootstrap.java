package ru.karamyshev.taskmanager.bootstrap;


import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.Map;

@Component
@Scope("singleton")
public class Bootstrap {

    @Autowired
    private ApplicationEventPublisher publisher;

    @Nullable
    private final Map<String, AbstractListener> commands = new LinkedHashMap<>();
    @Nullable
    private final Map<String, AbstractListener> arguments = new LinkedHashMap<>();

    public void run(@Nullable final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        inputCommand();
    }

    @SneakyThrows
    private void inputCommand() {
        while (true) {
            try {
                parsCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void parsCommand(@Nullable final String args) {
        validateArgs(args);
        String[] commandList = args.trim().split("\\s+");
        for (String command : commandList) {
            System.out.println("command " + command);
            publisher.publishEvent(new ConsoleEvent(command));
        }
    }

    private void parsArgs(final String... args) {
        validateArgs(args);
        try {
            for (String arg : args) publisher.publishEvent(new ConsoleEvent(arg));
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }

    }

    private void validateArgs(@Nullable final String... args) {
        if (args != null || args.length > 0) return;
        System.out.println(MsgCommandConst.COMMAND_ABSENT);
    }

}
