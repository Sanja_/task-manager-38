package ru.karamyshev.taskmanager.exception.empty;

public class LoginFailedException extends Exception {

    public LoginFailedException() {
        super("LOGIN FAILED...");
    }

}
