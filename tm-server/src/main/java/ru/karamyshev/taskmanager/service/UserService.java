package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.karamyshev.taskmanager.repository.IUserRepository;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.UserDTO;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.exception.NotLockYourUserException;
import ru.karamyshev.taskmanager.exception.NotMatchPasswordsException;
import ru.karamyshev.taskmanager.exception.NotRemoveYourUserException;
import ru.karamyshev.taskmanager.exception.empty.*;
import ru.karamyshev.taskmanager.exception.user.AccessDeniedException;
import ru.karamyshev.taskmanager.util.HashUtil;

import java.util.List;

@Service
public class UserService extends AbstractService<User> implements IUserService {

    @Autowired
     IUserRepository userRepository;

    @Nullable
    @Override
    public User findById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = userRepository.getOne(id);
        if (user == null) throw new Exception();
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginFailedException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new Exception();
        return user;
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        @NotNull final List<User> userList = userRepository.findAll();
        return UserDTO.toDTO(userList);
    }

    @Nullable
    @Override
    @Transactional
    public void removeUser(@Nullable final User user) throws Exception {
        if (user == null) throw new Exception();
        userRepository.delete(user);
    }

    @Nullable
    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        userRepository.deleteById(id);
    }

    @Nullable
    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginFailedException();
        userRepository.deleteByLogin(login);
    }

    @Nullable
    @Override
    @Transactional
    public void removeUserByLogin(
            @Nullable final String currentLogin,
            @Nullable final String login
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (currentLogin.equals(login)) throw new NotRemoveYourUserException();
        userRepository.deleteByLogin(login);
    }

    @Override
    @Transactional
    public void removeAllUsers() {
        userRepository.deleteAll();
    }

    @Nullable
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPassword = HashUtil.salt(password);
        if (saltPassword == null) return;
        user.setPasswordHash(saltPassword);
        userRepository.save(user);
    }

    @Nullable
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        user.setRole(Role.USER);
        @Nullable final String saltPassword = HashUtil.salt(password);
        if (saltPassword == null) return;
        user.setPasswordHash(saltPassword);
        userRepository.save(user);
    }

    @Nullable
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPassword = HashUtil.salt(password);
        if (saltPassword == null) return;
        user.setPasswordHash(saltPassword);
        user.setRole(role);
        userRepository.save(user);
    }

    @Nullable
    @Override
    @Transactional
    public void lockUserByLogin(
            @Nullable final String currentLogin,
            @Nullable final String login
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new Exception();
        if (currentLogin.equals(login)) throw new NotLockYourUserException();
        user.setLocked(true);
        userRepository.save(user);
    }

    @Nullable
    @Override
    @Transactional
    public void unlockUserByLogin(
            @Nullable final String currentLogin,
            @Nullable final String login
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        @Nullable final User currentUser = findByLogin(currentLogin);
        if (user == null) throw new Exception();
        if (currentUser == null) throw new Exception();
        if (currentLogin.equals(login)) throw new NotLockYourUserException();
        user.setLocked(false);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void renamePassword(
            @Nullable final String userId,
            @Nullable final String newPassword
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final String oldPassword = user.getPasswordHash();
        @Nullable final String newHash = HashUtil.salt(newPassword);
        if (newHash == null) throw new Exception();
        if (oldPassword.equals(newHash)) throw new NotMatchPasswordsException();
        user.setPasswordHash(newHash);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void renameFirstName(
            @Nullable final String currentUserId,
            @Nullable final String newFirstName
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new EmptyUserIdException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findById(currentUserId);
        if (user == null) throw new AccessDeniedException();
        user.setFirstName(newFirstName);
        userRepository.save(user);
    }

}
