package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.dto.UserDTO;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @WebMethod
    void createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) throws Exception;

    @WebMethod
    void createUserEmail(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws Exception;

    @Nullable
    @WebMethod
    void createUserRole(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) throws Exception;

    @Nullable
    @WebMethod
    User findUserById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

    @Nullable
    @WebMethod
    User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception;

    @WebMethod
    void removeUser(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "user", partName = "user") @Nullable User user
    ) throws Exception;

    @WebMethod
    void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

    @Nullable
    @WebMethod
    void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception;

    @Nullable
    @WebMethod
    void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception;

    @Nullable
    @WebMethod
    List<UserDTO> getUserList(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

    @WebMethod
    void renamePassword(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable String newPassword
    ) throws Exception;

    @WebMethod
    void loadUser(
            @WebParam(name = "users", partName = "users") @Nullable List<User> users,
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

}
